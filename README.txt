Sportipal

Sportipal est un logiciel de coaching sportif qui utilise le langage de programmation Python. 
Son objectif est d'aider les débutants comme les expérimentés à créer des séances de sport adaptées à leur niveau.

• L'application est utilisable uniquement sur ordinateur. Pour utiliser la fonction de reconnaissance de mouvement, 
une caméra est requise. Les systèmes d'exploitation compatibles sont : Windows 10 (recommandé) ou Windows 11.

• Pour lancer l'application sur ordinateur, certaines bibliothèques sont nécessaires. 
Leurs noms se trouvent dans le fichier "requirments.txt"

Ensuite lancer le programme "Sportipal.py"

