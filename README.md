
Il faut lancer le programme sportipal.py avec 
le système d'exploitation windows

## Installation

```console
# clone the repo
$ git clone https://gitlab.com/Syhkii/sportipal.git

# change the working directory to sherlock
$ cd sherlock

# install the requirements
$ python3 -m pip install -r requirements.txt
```